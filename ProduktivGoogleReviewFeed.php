<?php
    namespace ProduktivGoogleReviewFeed;

    class ProduktivGoogleReviewFeed {
        public $options;
        
        function __construct($options) {
            $this->options = $options;
        }
        
        function fetchGooglePlaceData() {            
            $instaApi = "https://maps.googleapis.com/maps/api/place/details/json?placeid={$this->options['place_id']}&key={$this->options['google_map_key']}";
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $instaApi);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;            
        }

        public function getGoogleReviewFeed() { 
            $googlePlaceDetails = json_decode($this->fetchGooglePlaceData(), true);
            return $googlePlaceDetails['result']['reviews'];
        }
    }
?>
